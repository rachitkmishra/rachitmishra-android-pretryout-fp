package com.raywenderlich.android.quicktip

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    // Call to set default values
    setDefaultValue()

    // Call to set click listener
    setClickListener()
  }

  /**
   * Function to set default value
   */
  private fun setDefaultValue() {
    tip_amount.setText("20")
    number_of_people.setText("4")
  }

  /**
   * Function to read user input
   */
  private fun readInput() {

    val billAmount = bill_amount.text.toString()
    val tipAmount = tip_amount.text.toString()
    val numberOfPeople = number_of_people.text.toString()

    val tipPerPerson = calculateTipPerPerson(
        billAmount = billAmount,
        tipAmount = tipAmount,
        numberOfPeople = numberOfPeople
    )

    showTip("$tipPerPerson")
  }

  private fun setClickListener() {

    calculate_tip.setOnClickListener {

      readInput()
    }
  }

  /**
   * Function to calculate tip
   */
  private fun calculateTipPerPerson(
      billAmount: String,
      tipAmount: String,
      numberOfPeople: String
  ): Int {

    return if (billAmount.isNotEmpty() && billAmount.toFloat() != 0f
        && tipAmount.isNotEmpty() && tipAmount.toFloat() != 0f
        && numberOfPeople.isNotEmpty() && numberOfPeople.toInt() != 0
    ) {

      val tip = (tipAmount.toFloat() / 100) * billAmount.toFloat()
      tip.roundToInt() / numberOfPeople.toInt()
    } else {

      0
    }
  }

  private fun showTip(tipPerPerson: String) {

    tip_text_view.text = tipPerPerson
  }
}
